//
//  CSViewController.m
//  CSListKit
//
//  Created by joy_yu on 04/12/2018.
//  Copyright (c) 2018 joy_yu. All rights reserved.
//

#import "CSViewController.h"
#import "CSDemoSectionManager.h"
#import "CSCellA.h"
#import "CSCellB.h"
#import "CSCellC.h"

@interface CSViewController ()<UIScrollViewDelegate,UICollectionViewDelegate>

@property (nonatomic,strong) UICollectionView *collectionView;

@property (nonatomic,strong) CSDemoSectionManager *sectionManager;

@end

@implementation CSViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.view addSubview:self.collectionView];
    self.sectionManager.collectionView = self.collectionView;
    self.sectionManager.scrollViewDelegate = self;
    self.sectionManager.collectionViewDelegate = self;
    [self.sectionManager loadData];
}

//sectionManager 可做，controller也可做的事情
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
 
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UICollectionView *)collectionView
{
    if (!_collectionView)
    {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height) collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.delegate = self.sectionManager;
        _collectionView.dataSource = self.sectionManager;
    }
    return _collectionView;
}

- (CSDemoSectionManager *)sectionManager
{
    if (!_sectionManager)
    {
        _sectionManager = [[CSDemoSectionManager alloc] init];
    }
    return _sectionManager;
}

@end
