//
//  CSCellC.h
//  CSListKit_Example
//
//  Created by joy_yu on 2018/4/13.
//  Copyright © 2018年 joy_yu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSListCell.h"

@interface CSCellC : CSListCell

@property (weak, nonatomic) IBOutlet UILabel *label;

@end
