//
//  CSDemoHeaderFooterView.h
//  CSListKit_Example
//
//  Created by 余强 on 2018/4/14.
//  Copyright © 2018年 289067005@qq.com. All rights reserved.
//

#import "CSListHeaderFooterView.h"

@interface CSDemoHeaderFooterView : CSListHeaderFooterView
@property (weak, nonatomic) IBOutlet UILabel *label;

@end
