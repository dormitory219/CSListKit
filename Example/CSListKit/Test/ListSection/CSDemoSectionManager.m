//
//  CSDemoSectionManager.m
//  CSListKit_Example
//
//  Created by joy_yu on 2018/4/13.
//  Copyright © 2018年 joy_yu. All rights reserved.
//

#import "CSDemoSectionManager.h"
#import "CSSectionA.h"
#import "CSSectionB.h"
#import "CSSectionC.h"

#import "CSModelA.h"
#import "CSModelB.h"
#import "CSModelC.h"

@implementation CSDemoSectionManager

- (void)loadData
{
    CSSectionA *sectionA = [CSSectionA sectionWithData:nil];
    sectionA.collectionView = self.collectionView;
    sectionA.viewController = self.viewController;
    sectionA.section = 0;
    
    NSMutableArray *items1 = [NSMutableArray arrayWithCapacity:1];
    CSModelA *modelA1 = [[CSModelA alloc] init];
    [items1 addObject:modelA1];
    
    CSModelA *modelA2 = [[CSModelA alloc] init];
    [items1 addObject:modelA2];
    
    CSModelA *modelA3 = [[CSModelA alloc] init];
    [items1 addObject:modelA3];
    sectionA.items = items1;
    
    [self addListSection:sectionA];
    
    
    CSSectionB *sectionB = [CSSectionB sectionWithData:nil];
    sectionB.collectionView = self.collectionView;
    sectionB.viewController = self.viewController;
    sectionB.section = 1;
    
    NSMutableArray *items2 = [NSMutableArray arrayWithCapacity:1];
    CSModelB *modelB1 = [[CSModelB alloc] init];
    [items2 addObject:modelB1];
    
    CSModelB *modelB2 = [[CSModelB alloc] init];
    [items2 addObject:modelB2];
    
    CSModelB *modelB3 = [[CSModelB alloc] init];
    [items2 addObject:modelB3];
    sectionB.items = items2;
    
    [self addListSection:sectionB];
    
    
    CSSectionC *sectionC = [CSSectionC sectionWithData:nil];
    sectionC.collectionView = self.collectionView;
    sectionC.viewController = self.viewController;
    sectionC.section = 2;
    
    NSMutableArray *items3 = [NSMutableArray arrayWithCapacity:1];
    CSModelC *modelC1 = [[CSModelC alloc] init];
    [items3 addObject:modelC1];
    
    CSModelC *modelC2 = [[CSModelC alloc] init];
    [items3 addObject:modelC2];
    
    CSModelC *modelC3 = [[CSModelC alloc] init];
    [items3 addObject:modelC3];
    
    CSModelC *modelC4 = [[CSModelC alloc] init];
    [items3 addObject:modelC4];
    
    CSModelC *modelC5 = [[CSModelC alloc] init];
    [items3 addObject:modelC5];
    sectionC.items = items3;

    [self addListSection:sectionC];
}

@end
