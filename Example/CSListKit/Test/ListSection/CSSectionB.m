//
//  CSSectionB.m
//  CSListKit_Example
//
//  Created by joy_yu on 2018/4/13.
//  Copyright © 2018年 joy_yu. All rights reserved.
//

#import "CSSectionB.h"
#import "CSModelB.h"
#import "CSCellB.h"
#import "CSDemoHeaderFooterView.h"

@interface CSSectionB()

@end

@implementation CSSectionB

+ (instancetype)adapterWithData:(id)data
{
    return [[self alloc] init];
}

- (void)setCollectionView:(UICollectionView *)collectionView
{
    [super setCollectionView:collectionView];
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([CSCellB class]) bundle:nil] forCellWithReuseIdentifier:[CSCellB reuseIdentifier]];
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([CSDemoHeaderFooterView class]) bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:[CSDemoHeaderFooterView headerReuseIdentifier]];
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([CSDemoHeaderFooterView class]) bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:[CSDemoHeaderFooterView footerReuseIdentifier]];
}

- (NSInteger)numberOfItems
{
    return self.items.count;
}

- (CGSize)sizeForItemAtIndex:(NSInteger)index
{
    return CGSizeMake([UIScreen mainScreen].bounds.size.width, 50);
}

- (__kindof UICollectionViewCell *)cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CSCellB *cell = [CSCellB cellWithCollectionView:self.collectionView atIndexPath:indexPath];
    cell.data = self.items[indexPath.item];
    cell.label.text = [NSString stringWithFormat:@"section:%ld,item:%ld",indexPath.section,indexPath.item];
    return cell;
}

- (__kindof UICollectionReusableView *)viewForSupplementaryElementOfKind:(NSString *)elementKind
                                                                 atIndex:(NSInteger)index
{
    CSDemoHeaderFooterView *view =  [CSDemoHeaderFooterView headerFooterViewWithCollectionView:self.collectionView atIndex:index supplementaryElementOfKind:elementKind];
    if ([elementKind isEqualToString:UICollectionElementKindSectionHeader])
    {
        view.backgroundColor = [UIColor purpleColor];
        view.label.text = [NSString stringWithFormat:@"header:%ld",index];
    }
    else if ([elementKind isEqualToString:UICollectionElementKindSectionFooter])
    {
        view.backgroundColor = [UIColor cyanColor];
        view.label.text = [NSString stringWithFormat:@"footer:%ld",index];
    }
    return view;
}

- (CGSize)sizeForSupplementaryViewOfKind:(NSString *)elementKind
                                 atIndex:(NSInteger)index
{
    return CGSizeMake([UIScreen mainScreen].bounds.size.width, 30);
}

@end
