//
//  CSSectionA.m
//  CSListKit_Example
//
//  Created by joy_yu on 2018/4/13.
//  Copyright © 2018年 joy_yu. All rights reserved.
//

#import "CSSectionA.h"
#import "CSModelA.h"
#import "CSCellA.h"
#import "CSDemoHeaderFooterView.h"

@interface CSSectionA()

@end

@implementation CSSectionA

+ (instancetype)adapterWithData:(id)data
{
    return [[self alloc] init];
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.minimumLineSpacing = 10.f;
        self.minimumInteritemSpacing = 10.f;
        self.inset = UIEdgeInsetsMake(10, 10, 10, 10);
    }
    return self;
}

- (void)setCollectionView:(UICollectionView *)collectionView
{
    [super setCollectionView:collectionView];
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([CSCellA class]) bundle:nil] forCellWithReuseIdentifier:[CSCellA reuseIdentifier]];
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([CSDemoHeaderFooterView class]) bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:[CSDemoHeaderFooterView headerReuseIdentifier]];
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([CSDemoHeaderFooterView class]) bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:[CSDemoHeaderFooterView footerReuseIdentifier]];
}

- (NSInteger)numberOfItems
{
    return self.items.count;
}

- (CGSize)sizeForItemAtIndex:(NSInteger)index
{
    return CGSizeMake(([UIScreen mainScreen].bounds.size.width-self.minimumLineSpacing*3)/2.f, 80);
}

- (__kindof UICollectionViewCell *)cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CSCellA *cell = [CSCellA cellWithCollectionView:self.collectionView atIndexPath:indexPath];
    cell.data = self.items[indexPath.item];
    cell.label.text = [NSString stringWithFormat:@"section:%ld,item:%ld",indexPath.section,indexPath.item];
    return cell;
}

- (void)didSelectItemAtIndex:(NSInteger)index
{
    NSLog(@"did select");
}

- (__kindof UICollectionReusableView *)viewForSupplementaryElementOfKind:(NSString *)elementKind
                                                                 atIndex:(NSInteger)index
{
    CSDemoHeaderFooterView *view =  [CSDemoHeaderFooterView headerFooterViewWithCollectionView:self.collectionView atIndex:index supplementaryElementOfKind:elementKind];
    
    if ([elementKind isEqualToString:UICollectionElementKindSectionHeader])
    {
        view.backgroundColor = [UIColor yellowColor];
        view.label.text = [NSString stringWithFormat:@"header:%ld",index];
    }
    else if ([elementKind isEqualToString:UICollectionElementKindSectionFooter])
    {
        view.backgroundColor = [UIColor redColor];
        view.label.text = [NSString stringWithFormat:@"footer:%ld",index];
    }
    return view;
}

- (CGSize)sizeForSupplementaryViewOfKind:(NSString *)elementKind
                                 atIndex:(NSInteger)index
{
    return CGSizeMake([UIScreen mainScreen].bounds.size.width, 30);
}

@end

