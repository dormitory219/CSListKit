# CSListKit

[![CI Status](http://img.shields.io/travis/joy_yu/CSListKit.svg?style=flat)](https://travis-ci.org/joy_yu/CSListKit)
[![Version](https://img.shields.io/cocoapods/v/CSListKit.svg?style=flat)](http://cocoapods.org/pods/CSListKit)
[![License](https://img.shields.io/cocoapods/l/CSListKit.svg?style=flat)](http://cocoapods.org/pods/CSListKit)
[![Platform](https://img.shields.io/cocoapods/p/CSListKit.svg?style=flat)](http://cocoapods.org/pods/CSListKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CSListKit is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'CSListKit'
```

## Author

joy_yu, qiang_yu@intsig.net

## License

CSListKit is available under the MIT license. See the LICENSE file for more info.
