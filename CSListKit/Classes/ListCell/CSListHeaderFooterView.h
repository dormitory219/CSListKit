//
//  CSListHeaderFooterView.h
//  CSListKit
//
//  Created by joy_yu on 2018/4/13.
//

#import <UIKit/UIKit.h>

@interface CSListHeaderFooterView : UICollectionReusableView

@property (nonatomic,weak) UICollectionView *collectionView;

+ (NSString *)headerReuseIdentifier;

+ (NSString *)footerReuseIdentifier;

+ (instancetype)headerFooterViewWithCollectionView:(__kindof UICollectionView *)collectionView
                                           atIndex:(NSInteger)index
                        supplementaryElementOfKind:(NSString *)elementKind;

@end
