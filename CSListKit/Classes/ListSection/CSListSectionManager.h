//
//  CSAdapterManager.h
//  CSListKit_Example
//
//  Created by joy_yu on 2018/4/12.
//  Copyright © 2018年 joy_yu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSListSection.h"

@interface CSListSectionManager : NSObject<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

// 分出collectionViewDelegate事件到外界处理
@property (nonatomic, weak) id <UICollectionViewDelegate> collectionViewDelegate;

// 分出scrollViewDelegate事件到外界处理
@property (nonatomic, weak) id <UIScrollViewDelegate> scrollViewDelegate;

@property (nonatomic,strong) NSMutableArray<__kindof CSListSection *> *sectionMap;

@property (nonatomic,weak) UIViewController *viewController;

@property (nonatomic,strong) UICollectionView *collectionView;

- (void)addListSection:(__kindof CSListSection *)listSection;

- (void)removeListSection:(__kindof CSListSection *)listSection;

@end
